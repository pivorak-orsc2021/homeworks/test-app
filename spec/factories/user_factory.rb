FactoryBot.define do
  factory :user do
    email { "test@email.com" }
    role { 'user' }

    trait :admin do
      role { 'admin' }
    end
  end
end
